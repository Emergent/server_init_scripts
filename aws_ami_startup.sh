#!/bin/bash
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>log.out 2>&1
yum update -y
yum install python37 -y
yum install git -y
yum install docker -y
#yum install libgomp -y
git clone https://github.com/VundleVim/Vundle.vim.git /home/ec2-user/.vim/bundle/Vundle.vim
git clone https://gitlab.com/Emergent/server_init_scripts.git /home/ec2-user/server_init
aws s3 cp s3://mboson/architecture_code/ssh_keys/git_rsa /home/ec2-user/.ssh/git_rsa
aws s3 cp s3://mboson/keys keys --recursive
cp /home/ec2-user/server_init/vim_code/vimrc /home/ec2-user/.vimrc
cp /home/ec2-user/server_init/bashrc /home/ec2-user/.bashrc
python3 -m venv /home/ec2-user/env/default
pip install --upgrade pip
chmod 400 /home/ec2-user/.ssh/git_rsa
chown -R ec2-user /home/ec2-user 
service docker start
chmod 666 /var/run/docker.sock
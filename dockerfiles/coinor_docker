#Dockerfile

FROM ubuntu:focal

# Install packages
RUN apt-get update -y
RUN DEBIAN_FRONTEND="noninteractive" apt-get -y install tzdata
ENV TZ=America/New_York
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
RUN apt-get install git -y
RUN apt-get install awscli -y
RUN apt-get install python3-pip -y
RUN apt-get install vim -y
RUN apt-get install python3-venv -y

# Install COINOR related packages & code
RUN apt-get -y install --no-install-recommends git subversion gcc g++ make wget gfortran patch pkg-config file
RUN apt-get -y install --no-install-recommends libgfortran5 libblas-dev liblapack-dev libmetis-dev libnauty2-dev
RUN apt-get -y install --no-install-recommends ca-certificates
RUN git clone https://github.com/coin-or/coinbrew /var/coin-or
WORKDIR /var/coin-or
RUN ./coinbrew fetch COIN-OR-OptimizationSuite@stable/1.9 --skip="ThirdParty/Blas ThirdParty/Lapack ThirdParty/Metis" --no-prompt
RUN ./coinbrew build  COIN-OR-OptimizationSuite --skip="ThirdParty/Blas ThirdParty/Lapack ThirdParty/Metis" --no-prompt --prefix=/usr

WORKDIR /root
RUN mkdir /root/.ssh
COPY git_rsa /root/.ssh/git_rsa
RUN git clone https://gitlab.com/Emergent/server_init_scripts.git /root/server_init
RUN cp /root/server_init/ubuntu_bashrc /root/.bashrc
RUN mkdir -p /root/.vim/pack/python-mode/start
RUN python3 -m venv /root/env/default
RUN git clone --recursive https://github.com/python-mode/python-mode.git /root/.vim/pack/python-mode/start/python-mode
RUN chmod 400 /root/.ssh/git_rsa
# RUN chown -R ubuntu /root
